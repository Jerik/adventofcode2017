#![allow(unknown_lints)]

mod aoc_helper;
mod days;

use crate::aoc_helper::core::Day;
use crate::days::*;

fn main() {
    day1::Day1::create("input_files/input1.txt").iter().for_each(Day::print);
    day2::Day2::create("input_files/input2.txt").iter().for_each(Day::print);
    day3::Day3::create(361_527).iter().for_each(Day::print);
    day4::Day4::create("input_files/input4.txt").iter().for_each(Day::print);
    day5::Day5::create("input_files/input5.txt").iter().for_each(Day::print);
    day6::Day6::create("input_files/input6.txt").iter().for_each(Day::print);
    day7::Day7::create("input_files/input7.txt").iter().for_each(Day::print);
    day8::Day8::create("input_files/input8.txt").iter().for_each(Day::print);
    day9::Day9::create("input_files/input9.txt").iter().for_each(Day::print);
    day10::Day10::create("input_files/input10.txt").iter().for_each(Day::print);
    day11::Day11::create("input_files/input11.txt").iter().for_each(Day::print);
    day12::Day12::create("input_files/input12.txt").iter().for_each(Day::print);
    day13::Day13::create("input_files/input13.txt").iter().for_each(Day::print);
    day14::Day14::create("nbysizxe".to_string()).iter().for_each(Day::print);
    day15::Day15::create("input_files/input15.txt").iter().for_each(Day::print);
    day16::Day16::create("input_files/input16.txt").iter().for_each(Day::print);
    day17::Day17::create(349).iter().for_each(Day::print);
    day18::Day18::create("input_files/input18.txt").iter().for_each(Day::print);
    day19::Day19::create("input_files/input19.txt").iter().for_each(Day::print);
    day20::Day20::create("input_files/input20.txt").iter().for_each(Day::print);
    day21::Day21::create("input_files/input21.txt").iter().for_each(Day::print);
    day22::Day22::create("input_files/input22.txt").iter().for_each(Day::print);
    day23::Day23::create("input_files/input23.txt").iter().for_each(Day::print);
    day24::Day24::create("input_files/input24.txt").iter().for_each(Day::print);
    day25::Day25::create("input_files/input25.txt").iter().for_each(Day::print);
}
