use std::{io, fmt, error::Error};

pub trait Day: Sized {
    const NUMBER: u32;
    type Input;
    type OutputPart1: fmt::Display;
    type OutputPart2: fmt::Display;

    fn create(input: Self::Input) -> DayCreationResult<Self>;

    fn get_solutions(&self) -> (Option<Self::OutputPart1>, Option<Self::OutputPart2>);

    fn print(&self) {
        println!("Day {}:", Self::NUMBER);

        let (option1, option2) = self.get_solutions();

        if let Some(solution) = option1 {
            println!("Part 1: {}", solution);
        }

        if let Some(solution) = option2 {
            println!("Part 2: {}", solution);
        }

        println!()
    }
}

pub type DayCreationResult<T> = Result<T, DayCreationError>;

#[derive(Debug)]
pub enum DayCreationError {
    Io(io::Error),
    ForbiddenValue,
}

impl fmt::Display for DayCreationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DayCreationError::Io(ref err) => err.fmt(f),
            DayCreationError::ForbiddenValue => write!(f, "Forbidden input value"),
        }
    }
}

impl Error for DayCreationError {}

impl From<io::Error> for DayCreationError {
    fn from(err: io::Error) -> Self {
        DayCreationError::Io(err)
    }
}
