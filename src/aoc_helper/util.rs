use std::{collections::{HashMap, HashSet, VecDeque}, hash::Hash};

pub const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

pub fn extract_group<T>(connection_map: &mut HashMap<T, Vec<T>>, representative: T) -> Option<HashSet<T>>
where T: Copy + PartialEq + Eq + Hash {
    if connection_map.contains_key(&representative) {
        let mut group: HashSet<T> = HashSet::new();
        let mut queue: VecDeque<T> = VecDeque::new();
        group.insert(representative);
        queue.push_front(representative);

        while let Some(member) = queue.pop_front() {
            for &connected_member in &connection_map[&member] {
                if group.insert(connected_member) {
                    queue.push_back(connected_member);
                }
            }
        }

        connection_map.retain(|k, _| !group.contains(k));

        Some(group)
    } else {
        None
    }
}
