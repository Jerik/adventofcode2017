use combine::{
    Parser, ParseError, Stream, optional, choice,
    many1, parser::char::{char, digit}
};

pub fn integer<I>() -> impl Parser<Input = I, Output = i32>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        optional(choice((char('+'), char('-')))),
        many1(digit()),
    ).map(|(sign, digits): (Option<char>, String)| {
        digits.parse::<i32>().unwrap() * match sign {
            Some('-') => -1,
            _ => 1,
        }
    })
}

pub fn unsigned_integer<I>() -> impl Parser<Input = I, Output = u32>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        many1(digit()),
    ).map(|(digits,): (String,)| {
        digits.parse::<u32>().unwrap()
    })
}
