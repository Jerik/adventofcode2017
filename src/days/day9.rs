use crate::aoc_helper::core::*;
use std::fs::read_to_string;

struct StreamProcessor {
    opened_groups: u32,
    closed_groups: u32,
    score: u32,
    garbage_amount: u32,
    inside_garbage: bool,
    ignore_next: bool,
}

impl StreamProcessor {
    fn new() -> StreamProcessor {
        StreamProcessor {
            opened_groups: 0,
            closed_groups: 0,
            score: 0,
            garbage_amount: 0,
            inside_garbage: false,
            ignore_next: false,
        }
    }

    fn get_stream_data(&mut self, stream: &str) -> (Option<u32>, Option<u32>) {
        stream.chars().for_each(|ch| self.process_token(ch));

        if self.opened_groups == self.closed_groups {
            (Some(self.score), Some(self.garbage_amount))
        } else {
            (None, None)
        }
    }

    fn process_token(&mut self, token: char) {
        if self.ignore_next {
            self.ignore_next = false;
        } else if self.inside_garbage {
            match token {
                '>' => self.inside_garbage = false,
                '!' => self.ignore_next = true,
                _ => self.garbage_amount += 1,
            }
        } else {
            match token {
                '{' => {
                    self.opened_groups += 1;
                    self.score += self.opened_groups - self.closed_groups;
                },
                '}' => self.closed_groups += 1,
                '<' => self.inside_garbage = true,
                _ => {},
            }
        }
    }
}

pub struct Day9 {
    stream: String,
}

impl Day for Day9 {
    const NUMBER: u32 = 9;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day9> {
        let stream = read_to_string(input)?;

        Ok(Day9 { stream })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        StreamProcessor::new().get_stream_data(&self.stream)
    }
}
