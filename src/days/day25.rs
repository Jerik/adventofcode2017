use crate::aoc_helper::{core::*, parse::unsigned_integer};
use std::{fs::read_to_string, collections::VecDeque};
use fxhash::FxHashMap;
use combine::{
    Parser, ParseError, Stream, choice, any, many1, attempt,
    parser::{char::{char, string}, repeat::skip_until}
};

enum Direction {
    Left,
    Right,
}

type Transition = (char, u8, Direction);

struct TuringMachine {
    tape: VecDeque<u8>,
    cursor_index: usize,
    current_state: char,
    state_transitions: FxHashMap<(char, u8), Transition>,
    steps: u32,
}

impl TuringMachine {
    fn new(intial_state: char, state_transitions: FxHashMap<(char, u8), Transition>, steps: u32) -> TuringMachine {
        TuringMachine {
            tape: [0].iter().cloned().collect(),
            cursor_index: 0,
            current_state: intial_state,
            state_transitions,
            steps,
        }
    }

    fn get_checksum(&mut self) -> u32 {
        for _ in 0..self.steps {
            self.make_step()
        }

        self.tape.iter().map(|&v| u32::from(v)).sum()
    }

    fn make_step(&mut self) {
        let (next_state, new_value, ref direction) =
            self.state_transitions[&(self.current_state, self.tape[self.cursor_index])];

        self.current_state = next_state;
        self.tape[self.cursor_index] = new_value;

        self.cursor_index = match direction {
            Direction::Left => {
                if self.cursor_index == 0 {
                    self.tape.push_front(0);

                    0
                } else {
                    self.cursor_index - 1
                }
            },
            Direction::Right => {
                if (self.cursor_index + 1) == self.tape.len() {
                    self.tape.push_back(0);
                }

                self.cursor_index + 1
            },
        };
    }
}

fn bit<I>() -> impl Parser<Input = I, Output = u8>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((char('0'), char('1'))).map(|bit| {
        match bit { '0' => 0, '1' => 1, _ => unreachable!() }
    })
}

fn direction<I>() -> impl Parser<Input = I, Output = Direction>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((string("left"), string("right"))).map(|direction| {
        match direction {
            "left" => Direction::Left,
            "right" => Direction::Right,
            _ => unreachable!()
        }
    })
}

fn transition_instructions<I>() -> impl Parser<Input = I, Output = Transition>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        skip_until(bit()).with(bit()),
        skip_until(attempt(direction())).with(direction()),
        skip_until(attempt(string("state "))).and(string("state ")).with(any()).skip(char('.'))
    ).map(|(bit, direction, next_state)| {
        (next_state, bit, direction)
    })
}

fn state_transition<I>() -> impl Parser<Input = I, Output = Vec<((char, u8), Transition)>>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        string("In state ").with(any()),
        skip_until(char('-')).with(transition_instructions()),
        skip_until(char('-')).with(transition_instructions()),
    ).map(|(state_name, instructions0, instructions1)| {
        vec![((state_name, 0), instructions0), ((state_name, 1), instructions1)]
    })
}

#[allow(clippy::type_complexity)]
fn turing_machine<I>() -> impl Parser<Input = I, Output = TuringMachine>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        string("Begin in state ").with(any()).skip(char('.')),
        string("Perform a diagnostic checksum after ").with(unsigned_integer()).skip(string(" steps.")),
        many1(state_transition())
    ).map(|(initial_state, steps, state_transitions): (_, _, Vec<Vec<((char, u8), Transition)>>)| {
        TuringMachine::new(initial_state, state_transitions.into_iter().flatten().collect(), steps)
    })
}

pub struct Day25 {
    turing_machine_notes: String,
}

impl Day for Day25 {
    const NUMBER: u32 = 25;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day25> {
        let turing_machine_notes = read_to_string(input)?
            .chars().filter(|&ch| ch != '\n').collect();

        Ok(Day25 { turing_machine_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let checksum = turing_machine::<&str>().parse(&self.turing_machine_notes).ok()
            .map(|(mut turing_machine, _)| { turing_machine.get_checksum() });

        (checksum, None)
    }
}
