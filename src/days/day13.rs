use crate::aoc_helper::{core::*, parse::unsigned_integer};
use std::fs::read_to_string;
use fxhash::FxHashMap;
use combine::{
    Parser, ParseError, Stream,
    parser::char::{space, char}
};

struct Firewall {
    layers: FxHashMap<u32, u32>,
    max_depth: Option<u32>,
}

impl Firewall {
    fn new(layers: FxHashMap<u32, u32>) -> Firewall {
        let max_depth = layers.keys().max().cloned();

        Firewall {
            layers,
            max_depth,
        }
    }

    fn get_scanner_position(&self, depth: u32, picoseconds: u32) -> Option<u32> {
        let range_minus_one = self.layers.get(&depth)?.checked_sub(1)?;
        let is_going_forward = (picoseconds / range_minus_one) % 2 == 0;
        let remainder = picoseconds % range_minus_one;

        Some( if is_going_forward { remainder } else { range_minus_one - remainder } )
    }

    fn is_caught_by_scanner(&self, depth: u32, picoseconds: u32) -> bool {
        match self.get_scanner_position(depth, picoseconds) {
            Some(0) => true,
            _ => false,
        }
    }

    fn calculate_severity(&self) -> u32 {
        if let Some(md) = self.max_depth {
            (0..=md).fold(0, |acc, depth| {
                acc + if self.is_caught_by_scanner(depth, depth) {
                    depth * self.layers[&depth]
                } else {
                    0
                }
            })
        } else {
            0
        }
    }

    fn find_smallest_delay(&self) -> u32 {
        if let Some(md) = self.max_depth {
            let mut current_depths = vec![Some(0)];
            let mut picoseconds = 0;

            loop {
                for depth_opt in &mut current_depths {
                    *depth_opt = depth_opt.filter(|depth| {
                        !self.is_caught_by_scanner(*depth, picoseconds)
                    });

                    if let Some(depth) = depth_opt {
                        if *depth > md {
                            return picoseconds - *depth;
                        }

                        *depth += 1;
                    }
                }

                current_depths.retain(|x| x.is_some());
                current_depths.push(Some(0));
                picoseconds += 1;
            }
        } else {
            0
        }
    }
}

fn firewall_layer<I>() -> impl Parser<Input = I, Output = (u32, u32)>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        unsigned_integer(),
        char(':').and(space())
        .with(unsigned_integer()),
    ).map(|(depth, range)| {
        (depth, range)
    })
}

pub struct Day13 {
    firewall_notes: String,
}

impl Day for Day13 {
    const NUMBER: u32 = 13;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day13> {
        let firewall_notes = read_to_string(input)?;

        Ok(Day13 { firewall_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let layers: FxHashMap<u32, u32> = self.firewall_notes.lines().filter_map(|line| {
            firewall_layer().parse(line).map(|(layer, _)| layer).ok()
        }).collect();

        let firewall = Firewall::new(layers);

        (Some(firewall.calculate_severity()), Some(firewall.find_smallest_delay()))
    }
}
