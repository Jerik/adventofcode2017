use crate::aoc_helper::{core::*, util::extract_group};
use crate::day10::get_knot_hash;
use std::collections::HashMap;
use hex;

pub struct Day14 {
    key_string: String,
}

impl Day for Day14 {
    const NUMBER: u32 = 14;
    type Input = String;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: String) -> DayCreationResult<Day14> {
        Ok(Day14 { key_string: input })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let table: Vec<Vec<char>> = (0..128).map(|n| {
            let key = format!("{}-{}", self.key_string, n);

            hex::decode(get_knot_hash(&key)).unwrap().iter().flat_map(|byte| {
                format!("{:08b}", byte).chars().collect::<Vec<char>>()
            }).collect()
        }).collect();

        let mut connection_map: HashMap<usize, Vec<usize>> = table.iter().enumerate().flat_map(|(i, row)| {
            row.iter().enumerate().filter(|(_, &ch)| ch == '1').map(|(j, _)| {
                let neighbours = [j.checked_sub(1).map(|y| (i, y)), Some((i, j + 1)),
                                  i.checked_sub(1).map(|x| (x, j)), Some((i + 1, j))];

                let used_neighbours: Vec<usize> = neighbours.iter().filter_map(|&opt| {
                    match opt {
                        Some((x, y)) if *table.get(x)?.get(y)? == '1' => Some(x * 128 + y),
                        _ => None,
                    }
                }).collect();

                (i * 128 + j, used_neighbours)
            }).collect::<Vec<(usize, Vec<usize>)>>()
        }).collect();

        let number_of_used_squares = connection_map.keys().count() as u32;
        let mut number_of_regions = 0;

        while let Some(&representative) = connection_map.keys().next() {
            extract_group(&mut connection_map, representative);
            number_of_regions += 1;
        }

        (Some(number_of_used_squares), Some(number_of_regions))
    }
}
