use crate::aoc_helper::core::*;
use std::{fs::read_to_string, collections::HashMap};

#[derive(PartialEq, Eq, Hash)]
struct Letters(Vec<char>);

impl Letters {
    fn new(word: &str) -> Letters {
        let mut letters: Vec<char> = word.chars().collect();
        letters.sort_unstable();

        Letters(letters)
    }
}

pub struct Day4 {
    passphrases: String,
}

impl Day for Day4 {
    const NUMBER: u32 = 4;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day4> {
        let passphrases = read_to_string(input)?;

        Ok(Day4 { passphrases })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let (sum1, sum2) = self.passphrases.lines().fold((0, 0), |(acc1, acc2), line| {
            let mut amounts1: HashMap<&str, u32> = HashMap::new();
            let mut amounts2: HashMap<Letters, u32> = HashMap::new();

            line.split_whitespace().for_each(|word| {
                let amount1 = amounts1.entry(word).or_insert(0);
                let amount2 = amounts2.entry(Letters::new(word)).or_insert(0);
                *amount1 += 1;
                *amount2 += 1;
            });

            (acc1 + match amounts1.values().max() { Some(&max) if max < 2 => 1, _ => 0 },
             acc2 + match amounts2.values().max() { Some(&max) if max < 2 => 1, _ => 0 })
        });

        (Some(sum1), Some(sum2))
    }
}
