use crate::aoc_helper::{core::*, parse::integer};
use std::{fs::read_to_string, collections::{HashMap, HashSet}};
use combine::{
    Parser, ParseError, Stream, between, count_min_max,
    optional, parser::char::{char, space, string}
};

#[derive(Debug)]
struct Particle {
    position: [i64; 3],
    velocity: [i64; 3],
    acceleration: [i64; 3],
    has_collided: bool,
}

impl Particle {
    fn new(position: [i64; 3], velocity: [i64; 3], acceleration: [i64; 3]) -> Particle {
        Particle {
            position,
            velocity,
            acceleration,
            has_collided: false,
        }
    }

    fn update(&mut self) {
        for i in 0..3 {
            self.velocity[i] += self.acceleration[i];
            self.position[i] += self.velocity[i];
        }
    }
}

fn particle<I>() -> impl Parser<Input = I, Output = Particle>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        string("p=").with(coordinates()),
        char(',').and(space()).and(string("v=")).with(coordinates()),
        char(',').and(space()).and(string("a=")).with(coordinates()),
    ).map(|(position, velocity, acceleration)| {
        Particle::new(position, velocity, acceleration)
    })
}

fn coordinates<I>() -> impl Parser<Input = I, Output = [i64; 3]>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    between(char('<'), char('>'), count_min_max(3, 3, integer().skip(optional(char(',')))))
    .map(|coord_vec: Vec<i32>| {
        [i64::from(coord_vec[0]), i64::from(coord_vec[1]), i64::from(coord_vec[2])]
    })
}

pub struct Day20 {
    particle_notes: String,
}

impl Day for Day20 {
    const NUMBER: u32 = 20;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day20> {
        let particle_notes = read_to_string(input)?;

        Ok(Day20 { particle_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut particles: Vec<Particle> = self.particle_notes.lines().filter_map(|line| {
            particle().parse(line).map(|(par, _)| par).ok()
        }).collect();

        // Simulate the first 1000 ticks. This does not solve the problems for arbitrary inputs.
        for _ in 0..1000 {
            let mut position_indices: HashMap<[i64; 3], Vec<usize>> = HashMap::new();

            for (i, particle) in particles.iter().enumerate().filter(|(_, par)| !par.has_collided) {
                position_indices.entry(particle.position).or_insert_with(Vec::new).push(i);
            }

            let collision_indices: HashSet<usize> = position_indices
                .values().filter(|pos_indices| pos_indices.len() > 1)
                .flat_map(|pos_indices| pos_indices).cloned().collect();

            for (i, par) in particles.iter_mut().enumerate() {
                if collision_indices.contains(&i) {
                    par.has_collided = true;
                }

                par.update();
            }
        }

        let closest_to_origin = particles.iter().enumerate().min_by_key(|(_, par)| {
            par.position[0].abs() + par.position[1].abs() + par.position[2].abs()
        }).map(|(i, _)| i as u32);

        let number_of_alive = particles.iter().filter(|par| !par.has_collided).count();

        (closest_to_origin, Some(number_of_alive as u32))
    }
}
