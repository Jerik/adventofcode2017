use crate::aoc_helper::{core::*, parse::integer};
use std::{fs::read_to_string, collections::HashMap};
use combine::{
    Parser, ParseError, Stream, choice, many1, attempt,
    parser::char::{space, letter, string}
};

struct Instruction {
    target_register: String,
    addition_amount: i32,
    check_condition: Box<dyn Fn(&mut HashMap<String, i32>) -> bool>,
}

impl Instruction {
    fn execute(&self, registers: &mut HashMap<String, i32>) -> Option<i32> {
        if (self.check_condition)(registers) {
            let value = registers.entry(self.target_register.clone()).or_insert(0);
            *value += self.addition_amount;

            Some(*value)
        } else {
            None
        }
    }
}

fn instruction<I>() -> impl Parser<Input = I, Output = Instruction>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        many1(letter()),
        space().with(choice((string("inc"), string("dec")))),
        space().with(integer()),
        space().and(string("if")).and(space()).with(many1(letter())),
        space().with(choice((
            attempt(string("==")), attempt(string("!=")), attempt(string(">=")),
            attempt(string("<=")), attempt(string(">")), attempt(string("<")),
        ))),
        space().with(integer()),
    ).map(|(target_register, operation, amount, condition_register, comparision_operator,
    comparison_value): (String, &str, i32, String, &str, i32 )| {
        Instruction {
            target_register,
            addition_amount: amount * if operation == "dec" { -1 } else { 1 },
            check_condition: Box::new(move |registers: &mut HashMap<String, i32>| {
                let condition_value = *registers.entry(condition_register.clone()).or_insert(0);

                match comparision_operator {
                    "==" => condition_value == comparison_value,
                    "!=" => condition_value != comparison_value,
                    ">=" => condition_value >= comparison_value,
                    "<=" => condition_value <= comparison_value,
                    ">" => condition_value > comparison_value,
                    "<" => condition_value < comparison_value,
                    _ => unreachable!(),
                }
            }),
        }
    })
}

pub struct Day8 {
    instruction_notes: String,
}

impl Day for Day8 {
    const NUMBER: u32 = 8;
    type Input = &'static str;
    type OutputPart1 = i32;
    type OutputPart2 = i32;

    fn create(input: &str) -> DayCreationResult<Day8> {
        let instruction_notes = read_to_string(input)?;

        Ok(Day8 { instruction_notes })
    }

    fn get_solutions(&self) -> (Option<i32>, Option<i32>) {
        let mut registers: HashMap<String, i32> = HashMap::new();

        let all_time_max = self.instruction_notes.lines().filter_map(|line| {
            instruction().parse(line).map(|(instr, _)| instr).ok()
        }).fold(0, |max, instr| {
            match instr.execute(&mut registers) {
                Some(new_value) if new_value > max => new_value,
                _ => max,
            }
        });

        (registers.values().max().cloned(), Some(all_time_max))
    }
}
