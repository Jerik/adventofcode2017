use crate::aoc_helper::core::*;
use std::{fs::read_to_string, collections::HashMap, marker::PhantomData};
use fxhash::FxHashMap;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn new(x: i32, y: i32) -> Position {
        Position { x, y }
    } 

    fn move_into_dir(&mut self, direction: &Direction) {
        match direction {
            Direction::Up => self.y += 1,
            Direction::Down => self.y -= 1,
            Direction::Left => self.x -= 1,
            Direction::Right => self.x += 1,
        }
    }
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn turn_right(&mut self) {
        *self = match self {
            Direction::Up => Direction::Right,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Right => Direction::Down,
        }
    }

    fn turn_left(&mut self) {
        *self = match self {
            Direction::Up => Direction::Left,
            Direction::Down => Direction::Right,
            Direction::Left => Direction::Down,
            Direction::Right => Direction::Up,
        }
    }

    fn reverse(&mut self) {
        *self = match self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        }
    }
}

#[derive(Clone)]
enum NodeStatus {
    Clean,
    Weakened,
    Infected,
    Flagged,
}

trait Part {}
struct Part1 {}
struct Part2 {}
impl Part for Part1 {}
impl Part for Part2 {}

struct Grid<P: Part> {
    node_map: FxHashMap<Position, NodeStatus>,
    current_position: Position,
    direction: Direction,
    num_of_caused_infections: u32,
    phantom: PhantomData<*const P>,
}

impl<P: Part> Grid<P> {
    fn new(node_map: FxHashMap<Position, NodeStatus>, starting_pos: Position) -> Grid<P> {
        Grid {
            node_map,
            current_position: starting_pos,
            direction: Direction::Up,
            num_of_caused_infections: 0,
            phantom: PhantomData,
        }
    }

    fn get_num_of_caused_infections(&self) -> u32 {
        self.num_of_caused_infections
    }
}

impl Grid<Part1> {
    fn virus_burst(&mut self) {
        let current_node_status = self.node_map.entry(self.current_position).or_insert(NodeStatus::Clean);

        *current_node_status = match current_node_status {
            NodeStatus::Clean => {
                self.direction.turn_left();
                self.num_of_caused_infections += 1;
                NodeStatus::Infected
            },
            NodeStatus::Infected => {
                self.direction.turn_right();
                NodeStatus::Clean
            },
            _ => unreachable!(),
        };

        self.current_position.move_into_dir(&self.direction);
    }
}

impl Grid<Part2> {
    fn virus_burst(&mut self) {
        let current_node_status = self.node_map.entry(self.current_position).or_insert(NodeStatus::Clean);

        *current_node_status = match current_node_status {
            NodeStatus::Clean => {
                self.direction.turn_left();
                NodeStatus::Weakened
            },
            NodeStatus::Weakened => {
                self.num_of_caused_infections += 1;
                NodeStatus::Infected
            },
            NodeStatus::Infected => {
                self.direction.turn_right();
                NodeStatus::Flagged
            },
            NodeStatus::Flagged => {
                self.direction.reverse();
                NodeStatus::Clean
            }
        };

        self.current_position.move_into_dir(&self.direction);
    }
}

pub struct Day22 {
    grid_center: String,
}

impl Day for Day22 {
    const NUMBER: u32 = 22;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day22> {
        let grid_center = read_to_string(input)?;

        Ok(Day22 { grid_center })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut node_map: FxHashMap<Position, NodeStatus> = HashMap::default();

        for (i, line) in self.grid_center.lines().enumerate() {
            for (j, ch) in line.chars().enumerate() {
                match ch {
                    '.' => { node_map.insert(Position::new(j as i32, - (i as i32)), NodeStatus::Clean); },
                    '#' => { node_map.insert(Position::new(j as i32, - (i as i32)), NodeStatus::Infected); },
                    _ => {},
                }
            }
        }

        let starting_pos = {
            let height = self.grid_center.lines().count();
            let width = match self.grid_center.lines().nth(height / 2) {
                Some(line) => line.chars().count(),
                None => return (None, None),
            };

            Position::new(height as i32 / 2, - (width as i32) / 2)
        };

        let mut grid1: Grid<Part1> = Grid::new(node_map.clone(), starting_pos);
        let mut grid2: Grid<Part2> = Grid::new(node_map, starting_pos);

        for _ in 0..10000 {
            grid1.virus_burst();
        }

        for _ in 0..10_000_000 {
            grid2.virus_burst();
        }

        (Some(grid1.get_num_of_caused_infections()), Some(grid2.get_num_of_caused_infections()))
    }
}
