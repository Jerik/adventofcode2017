use crate::aoc_helper::core::*;
use std::{fs::read_to_string, collections::HashMap};
use indextree::{Arena, NodeId};
use failure::{Fallible, format_err};

struct Program {
    name: String,
    weight: u32,
}

struct ProgramTowerBuilder {
    arena: Arena<Program>,
    id_map: HashMap<String, NodeId>,
}

impl ProgramTowerBuilder {
    fn new() -> ProgramTowerBuilder {
        ProgramTowerBuilder {
            arena: Arena::new(),
            id_map: HashMap::new(),
        }
    }

    fn add_program(&mut self, name: String, weight: u32) {
        if self.id_map.get(&name).is_none() {
            let id = self.arena.new_node(Program { name: name.clone(), weight });
            self.id_map.insert(name, id);
        }
    }

    fn connect_programs(&mut self, parent_name: &str, child_name: &str) -> Fallible<()> {
        let parent_id = self.id_map.get(parent_name).ok_or_else(|| format_err!("No program called {}", parent_name))?;
        let child_id = self.id_map.get(child_name).ok_or_else(|| format_err!("No program called {}", child_name))?;

        parent_id.append(*child_id, &mut self.arena)
    }

    fn get_root_id(&self) -> Option<NodeId> {
        let candidates: Vec<&NodeId> = self.id_map.values().filter(|&&id| {
            match self.arena.get(id) {
                Some(node) => node.parent().is_none(),
                None => false,
            }
        }).collect();

        match candidates.len() {
            1 => Some(*candidates[0]),
            _ => None,
        }
    }

    fn calculate_subtower_weights(&self, node_id: NodeId, subtower_weights: &mut HashMap<NodeId, u32>) {
        let weight = self.arena.get(node_id).unwrap().data.weight;

        let subtower_weight = weight + node_id.children(&self.arena).map(|id| {
            self.calculate_subtower_weights(id, subtower_weights);
            subtower_weights[&id]
        }).sum::<u32>();

        subtower_weights.insert(node_id, subtower_weight);
    }

    fn build(self) -> Option<ProgramTower> {
        let root_id = self.get_root_id()?;

        let mut subtower_weights: HashMap<NodeId, u32> = HashMap::new();
        self.calculate_subtower_weights(root_id, &mut subtower_weights);

        Some(ProgramTower {
            arena: self.arena,
            root_id,
            subtower_weights,
        })
    }
}

struct ProgramTower {
    arena: Arena<Program>,
    root_id: NodeId,
    subtower_weights: HashMap<NodeId, u32>,
}

impl ProgramTower {
    fn get_root_name(&self) -> &str {
        &self.arena.get(self.root_id).unwrap().data.name
    }

    fn get_balanced_weight(&self) -> Option<u32> {
        let mut unbalanced_id = self.root_id;
        let mut balanced_subtower_weight = 0;

        loop {
            let children: Vec<NodeId> = unbalanced_id.children(&self.arena).collect();

            if children.len() <= 1 {
                break None;
            }

            let reference_value = self.subtower_weights[&children[0]];

            let (left, right): (Vec<NodeId>, Vec<NodeId>) = children.iter()
                .partition(|id| self.subtower_weights[id] == reference_value);

            if left.len() == 1 {
                unbalanced_id = left[0];
                balanced_subtower_weight = self.subtower_weights[&right[0]];
            } else if right.len() == 1 {
                unbalanced_id = right[0];
                balanced_subtower_weight = self.subtower_weights[&left[0]];
            } else {
                break Some(balanced_subtower_weight - reference_value * children.len() as u32);
            }
        }
    }
}

pub struct Day7 {
    program_notes: String,
}

impl Day for Day7 {
    const NUMBER: u32 = 7;
    type Input = &'static str;
    type OutputPart1 = String;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day7> {
        let program_notes = read_to_string(input)?;

        Ok(Day7 { program_notes })
    }

    fn get_solutions(&self) -> (Option<String>, Option<u32>) {
        let mut program_tower_builder = ProgramTowerBuilder::new();
        let mut parents_and_children: Vec<(String, Vec<String>)> = Vec::new();

        for line in self.program_notes.lines() {
            let trim: &[char] = &['(', ')', ','];
            let words: Vec<String> = line.split_whitespace().filter(|&s| s != "->")
                                         .map(|s| s.trim_matches(trim).to_string()).collect();

            if words.len() >= 2 {
                if let Ok(weight) = words[1].parse() {
                    program_tower_builder.add_program(words[0].clone(), weight);

                    if !words[2..].is_empty() {
                        parents_and_children.push((words[0].clone(), words[2..].to_vec()));
                    }
                }
            }
        }

        for (parent_name, children_names) in &parents_and_children {
            for child_name in children_names {
                if let Err(e) = program_tower_builder.connect_programs(parent_name, child_name) {
                    println!("{}", e);
                };
            }
        }

        if let Some(program_tower) = program_tower_builder.build() {
            (Some(program_tower.get_root_name().to_string()), program_tower.get_balanced_weight())
        } else {
            (None, None)
        }
    }
}
