use crate::aoc_helper::core::*;
use std::fs::read_to_string;
use fxhash::FxHashMap;

#[derive(Clone, PartialEq, Eq, Hash)]
struct Pattern {
    size: usize,
    pixels: Vec<Vec<char>>,
}

impl Pattern {
    fn new(size: usize, pixels: Vec<Vec<char>>) -> Pattern {
        Pattern { size, pixels, }
    }

    fn new_from_strings<I>(size: usize, strings: I) -> Pattern
    where I: IntoIterator, <I as IntoIterator>::Item: AsRef<str> {
        let pixels = strings.into_iter().map(|s| s.as_ref().chars().collect()).collect();

        Pattern { size, pixels, }
    }

    fn count_pixels_on(&self) -> u32 {
        self.pixels.iter().flatten().filter(|&&ch| ch == '#').count() as u32
    }

    fn split_into_chunks(&self, chunk_size: usize) -> Option<Vec<Vec<Pattern>>> {
        if chunk_size == 0 || self.size % chunk_size != 0 {
            return None;
        }

        let chunks = (0..self.size).step_by(chunk_size).map(|i| {
            (0..self.size).step_by(chunk_size).map(|j| {
                let chunk_pixels: Vec<Vec<char>> = (0..chunk_size).map(|i_offset| {
                    (0..chunk_size).map(|j_offset| self.pixels[i+i_offset][j+j_offset]).collect()
                }).collect();

                Pattern::new(chunk_size, chunk_pixels)
            }).collect()
        }).collect();

        Some(chunks)
    }

    fn assemble_from_chunks(chunks: Vec<Vec<Pattern>>) -> Option<Pattern> {
        let chunk_size = chunks.get(0)?.get(0)?.size;
        let size = chunks.len() * chunk_size;

        let pixels: Vec<Vec<char>> = (0..size).map(move |n| {
            (0..size).map(|m| {
                let (i, j) = (n / chunk_size, m / chunk_size);
                let (x, y) = (n % chunk_size, m % chunk_size);

                chunks[i][j].pixels[x][y]
            }).collect()
        }).collect();

        Some(Pattern::new(size, pixels))
    }

    #[allow(clippy::many_single_char_names)]
    fn get_permutations(&self) -> Option<Vec<Pattern>> {
        if self.size == 2 {
            let (a, b, c, d) = (self.pixels[0][0], self.pixels[0][1], self.pixels[1][0], self.pixels[1][1]);

            Some([
                    (a, b, c, d),
                    (c, a, d, b),
                    (d, c, b, a),
                    (b, d, a, c),
                    (b, a, d, c),
                    (d, b, c, a),
                    (c, d, a, b),
                    (a, c, b, d)
                ].iter().map(|&(ch1, ch2, ch3, ch4)| {
                    Pattern::new(2, vec![vec![ch1, ch2], vec![ch3, ch4]])
                }).collect()
            )
        } else if self.size == 3 {
            let (a, b, c, d, e, f, g, h, i) = (self.pixels[0][0], self.pixels[0][1], self.pixels[0][2], self.pixels[1][0],
                self.pixels[1][1], self.pixels[1][2], self.pixels[2][0], self.pixels[2][1], self.pixels[2][2]);

            Some([
                    (a, b, c, d, e, f, g, h, i),
                    (g, d, a, h, e, b, i, f, c),
                    (i, h, g, f, e, d, c, b, a),
                    (c, f, i, b, e, h, a, d, g),
                    (c, b, a, f, e, d, i, h, g),
                    (a, d, g, b, e, h, c, f, i),
                    (g, h, i, d, e, f, a, b, c),
                    (i, f, c, h, e, b, g, d, a)
                ].iter().map(|&(ch1, ch2, ch3, ch4, ch5, ch6, ch7, ch8, ch9)| {
                    Pattern::new(3, vec![vec![ch1, ch2, ch3], vec![ch4, ch5, ch6], vec![ch7, ch8, ch9]])
                }).collect()
            )
        } else {
            None
        }
    }
}

struct Rule {
    origin_pattern: Pattern,
    output_pattern: Pattern,
}

impl Rule {
    fn new(origin_pattern: Pattern, output_pattern: Pattern) -> Rule {
        Rule { origin_pattern, output_pattern, }
    }

    fn apply(&self, pattern: &Pattern) -> Option<Pattern> {
        if pattern == &self.origin_pattern {
            Some(self.output_pattern.clone())
        } else {
            None
        }
    }

    fn find_applicable_and_apply(&self, patterns: &[Pattern]) -> Option<Pattern> {
        for pattern in patterns {
            let result = self.apply(pattern);

            if result.is_some() {
                return result;
            }
        }

        None
    }
}

struct Enhancer {
    rules: Vec<Rule>,
    enhancements: FxHashMap<Pattern, Pattern>,
}

impl Enhancer {
    fn new(rules: Vec<Rule>) -> Enhancer {
        Enhancer {
            rules,
            enhancements: FxHashMap::default(),
        }
    }

    fn iterate_and_count_pixels_on(&mut self, mut pattern: Pattern, iterations: &[u32]) -> Option<Vec<u32>> {
        let max = *iterations.iter().max()?;
        let mut result = Vec::with_capacity(iterations.len());

        for i in 1..=max {
            pattern = self.enhance(&pattern)?;

            if iterations.contains(&i) {
                result.push(pattern.count_pixels_on())
            }
        }

        Some(result)
    }

    fn enhance(&mut self, pattern: &Pattern) -> Option<Pattern> {
        if pattern.size == 2 || pattern.size == 3 {
            if self.enhancements.contains_key(&pattern) {
                self.enhancements.get(&pattern).cloned()
            } else {
                let permutations = pattern.get_permutations()?;

                let enhanced_pattern = self.rules.iter().map(|rule| {
                    rule.find_applicable_and_apply(&permutations)
                }).find(|p| p.is_some())?;

                for perm in permutations {
                    self.enhancements.insert(perm, enhanced_pattern.clone().unwrap());
                }

                enhanced_pattern
            }
        } else {
            let chunks = pattern.split_into_chunks(2).or_else(|| pattern.split_into_chunks(3))?;

            let enhanced_chunks = chunks.into_iter().map(|chunk_row| {
                chunk_row.into_iter().filter_map(|chunk| self.enhance(&chunk)).collect()
            }).collect();

            Pattern::assemble_from_chunks(enhanced_chunks)
        }
    } 
}

pub struct Day21 {
    enhancement_rules: String,
}

impl Day for Day21 {
    const NUMBER: u32 = 21;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day21> {
        let enhancement_rules = read_to_string(input)?;

        Ok(Day21 { enhancement_rules })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let rules: Vec<Rule> = self.enhancement_rules.lines().filter_map(|line| {
            let (origin_raw, output_raw): (&str, &str) = {
                let mut split = line.split(" => ");

                (split.next()?, split.next()?)
            };

            let get_pattern = |raw: &str| {
                let size = raw.chars().position(|ch| ch == '/')?;
                let strings = raw.split_terminator('/');

                Some(Pattern::new_from_strings(size, strings))
            };

            Some(Rule::new(get_pattern(origin_raw)?, get_pattern(output_raw)?))
        }).collect();

        let mut enhancer = Enhancer::new(rules);
        let start_pattern = Pattern::new_from_strings(3, &[".#.", "..#", "###"]);
        let pixel_numbers = enhancer.iterate_and_count_pixels_on(start_pattern, &[5, 18]);

        if let Some(nums) = pixel_numbers {
            (nums.get(0).cloned(), nums.get(1).cloned())
        } else {
            (None, None)
        }
    }
}
