use crate::aoc_helper::core::*;
use std::collections::HashMap;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn new(x: i32, y: i32) -> Position {
        Position { x, y }
    }

    fn get_neighbours(self) -> [Position; 8] {
        [Position::new(self.x - 1, self.y - 1),
        Position::new(self.x, self.y - 1),
        Position::new(self.x + 1, self.y - 1),
        Position::new(self.x - 1, self.y),
        Position::new(self.x + 1, self.y),
        Position::new(self.x - 1, self.y + 1),
        Position::new(self.x, self.y + 1),
        Position::new(self.x + 1, self.y + 1)]
    }
}

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

struct Spiral {
    value_map: HashMap<Position, u32>,
    current_position: Position,
    current_layer: u32,
    direction: Direction,
    steps_until_turn: u32,
}

impl Spiral {
    fn init() -> Spiral {
        let current_position = Position::new(0, 0);
        let value_map = [(current_position, 1)].iter().cloned().collect();

        Spiral {
            value_map,
            current_position,
            current_layer: 0,
            direction: Direction::Right,
            steps_until_turn: 1,
        }
    }

    fn take_step(&mut self) {
        self.current_position = match self.direction {
            Direction::Up => Position::new(self.current_position.x, self.current_position.y + 1),
            Direction::Down => Position::new(self.current_position.x, self.current_position.y - 1),
            Direction::Left => Position::new(self.current_position.x - 1, self.current_position.y),
            Direction::Right => Position::new(self.current_position.x + 1, self.current_position.y),
        };

        self.steps_until_turn -= 1;
        
        if self.steps_until_turn == 0 {
            match self.direction {
                Direction::Up => {
                    self.direction = Direction::Left;
                    self.steps_until_turn = 2 * self.current_layer;
                },
                Direction::Left => {
                    self.direction = Direction::Down;
                    self.steps_until_turn = 2 * self.current_layer;
                },
                Direction::Down => {
                    self.direction = Direction::Right;
                    self.steps_until_turn = 2 * self.current_layer + 1;
                },
                Direction::Right => {
                    self.current_layer += 1;
                    self.direction = Direction::Up;
                    self.steps_until_turn = 2 * self.current_layer - 1;
                },
            }
        }
    }
}

impl Iterator for Spiral {
    type Item = u32;

    fn next(&mut self) -> Option<u32> {
        if self.current_position == Position::new(0, 0) {
            self.take_step();

            Some(1)
        } else {
            let neighbours = self.current_position.get_neighbours();
            let value = neighbours.iter().filter_map(|pos| self.value_map.get(pos)).sum();
            self.value_map.insert(self.current_position, value);
            self.take_step();

            Some(value)
        }
    }
}

pub struct Day3 {
    square: u32,
}

impl Day for Day3 {
    const NUMBER: u32 = 3;
    type Input = u32;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: u32) -> DayCreationResult<Day3> {
        if input == 0 {
            Err(DayCreationError::ForbiddenValue)
        } else {
            Ok(Day3 { square: input })
        }
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let solution1 = if self.square == 1 {
            0
        } else {
            let layer = ((self.square as f32).sqrt().ceil() as u32) / 2;
            let layer_anchor = (2 * layer - 1).pow(2);

            let distance_from_closest_center = &[1, 3, 5, 7].iter().map(|&k| {
                let center = layer_anchor + k * layer;

                if center >= self.square { center - self.square } else { self.square - center }
            }).min().unwrap();

            layer + distance_from_closest_center
        };

        let solution2 = Spiral::init().find(|&n| n > self.square).unwrap();

        (Some(solution1), Some(solution2))
    }
}
