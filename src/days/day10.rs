use crate::aoc_helper::core::*;
use std::fs::read_to_string;

fn get_knotted_list(lengths: &[u8], rounds: u32) -> Vec<u8> {
    let mut list: Vec<u8> = (0..=255).collect();
    let mut current_position = 0;
    let mut skip_size = 0;

    for _ in 0..rounds {
        for &length in lengths {
            let end = current_position + length as usize;

            if end <= 255 {
                list[current_position..end].reverse();
            } else {
                let offset = end - 255;

                list.rotate_left(offset);
                list[current_position-offset..255].reverse();
                list.rotate_right(offset);
            }

            current_position = (current_position + length as usize + skip_size) % 256;
            skip_size += 1;
        }
    }

    list
}

pub fn get_knot_hash(key: &str) -> String {
    let ascii_lengths: Vec<u8> = key.trim().chars()
        .filter(|ch| ch.is_ascii()).map(|ch| ch as u8)
        .chain([17, 31, 73, 47, 23].iter().cloned()).collect();
        
    get_knotted_list(&ascii_lengths, 64).chunks(16).map(|chunk| {
        format!("{:02x}", chunk[1..].iter().fold(chunk[0], |x, y| x ^ y))
    }).collect()
}

pub struct Day10 {
    lengths_notes: String,
}

impl Day for Day10 {
    const NUMBER: u32 = 10;
    type Input = &'static str;
    type OutputPart1 = u16;
    type OutputPart2 = String;

    fn create(input: &str) -> DayCreationResult<Day10> {
        let lengths_notes = read_to_string(input)?;

        Ok(Day10 { lengths_notes })
    }

    fn get_solutions(&self) -> (Option<u16>, Option<String>) {
        let lengths: Vec<u8> = self.lengths_notes.trim().split_terminator(',')
            .filter_map(|s| s.parse().ok()).collect();

        let product_opt = if lengths.is_empty() {
            None
        } else {
            let list = get_knotted_list(&lengths, 1);

            Some(u16::from(list[0]) * u16::from(list[1]))
        };

        (product_opt, Some(get_knot_hash(&self.lengths_notes)))
    }
}
