use crate::aoc_helper::core::*;
use std::fs::read_to_string;

fn count_steps(mut jumper: Vec<i32>, part2: bool) -> u32 {
    let mut counter = 0;
    let mut index = 0;

    while let Some(offset) = jumper.get_mut(index) {
        counter += 1;

        if index as i32 + *offset < 0 {
            break;
        } else {
            index = (index as i32 + *offset) as usize;

            if part2 && *offset >= 3 {
                *offset -= 1;
            } else {
                *offset += 1;
            }
        }
    }

    counter
}

pub struct Day5 {
    offsets: String,
}

impl Day for Day5 {
    const NUMBER: u32 = 5;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day5> {
        let offsets = read_to_string(input)?;

        Ok(Day5 { offsets })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let jumper: Vec<i32> = self.offsets.lines().filter_map(|line| line.parse().ok()).collect();

        (Some(count_steps(jumper.clone(), false)), Some(count_steps(jumper, true)))
    }
}
