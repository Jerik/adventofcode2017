use crate::aoc_helper::{core::*, util::ALPHABET, parse::unsigned_integer};
use std::fs::read_to_string;
use combine::{
    Parser, ParseError, Stream, choice,
    sep_by, parser::char::{letter, char}
};

fn get_position(ch: char) -> Option<usize> {
    ALPHABET[0..16].chars().position(|x| x == ch)
}

fn inverse(perm: &[usize]) -> Vec<usize> {
    let mut inverse: Vec<(usize, usize)> = perm.iter().cloned().enumerate().collect();
    inverse.sort_unstable_by_key(|&(_, j)| j);

    inverse.iter().map(|&(i, _)| i).collect()
}

fn composition(first_perm: &[usize], second_perm: &[usize]) -> Vec<usize> {
    first_perm.iter().map(|&x| second_perm[x]).collect()
}

fn iterated_self_composition(base_perm: &[usize], mut result_perm: Vec<usize>, n: u32) -> Vec<usize> {
    if n == 0 {
        result_perm
    } else {
        if n & 1 == 1 {
            result_perm = composition(&result_perm, &base_perm);
        }

        iterated_self_composition(&composition(&base_perm, &base_perm), result_perm, n / 2)
    }
}

struct Choreography {
    index_perm: Vec<usize>,
    inv_character_perm: Vec<usize>,
}

impl Choreography {
    fn starting_formation() -> Choreography {
        Choreography {
            index_perm: (0..16).collect(),
            inv_character_perm: (0..16).collect(),
        }
    }

    fn add_move(&mut self, dance_move: &DanceMove) {
        match *dance_move {
            DanceMove::Spin(x) => { self.index_perm.rotate_right(x); },
            DanceMove::Exchange(i, j) => {
                if i < 16 && j < 16 {
                    self.index_perm.swap(i, j);
                }
            },
            DanceMove::Partner(a, b) => {
                if let (Some(i), Some(j)) = (get_position(a), get_position(b)) {
                    self.inv_character_perm.swap(i, j);
                }
            },
        }
    }

    fn dance(&self, n: u32) -> String {
        let index_perm_n = iterated_self_composition(&self.index_perm, (0..16).collect(), n);
        let character_perm_n = iterated_self_composition(&inverse(&self.inv_character_perm), (0..16).collect(), n);
        let complete_perm = composition(&index_perm_n, &character_perm_n);

        let letters: Vec<char> = ALPHABET[0..16].chars().collect();

        complete_perm.iter().map(|&x| letters[x]).collect()
    }
}

enum DanceMove {
    Spin(usize),
    Exchange(usize, usize),
    Partner(char, char),
}

fn dance_move<I>() -> impl Parser<Input = I, Output = DanceMove>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((
        char('s').with(unsigned_integer()).map(|num| DanceMove::Spin(num as usize)),
        char('x').with(unsigned_integer().skip(char('/')).and(unsigned_integer())).map(|(a, b)| {
            DanceMove::Exchange(a as usize, b as usize)
        }),
        char('p').with(letter().skip(char('/')).and(letter())).map(|(a, b)| DanceMove::Partner(a, b))
    ))
}

pub struct Day16 {
    dance_notes: String,
}

impl Day for Day16 {
    const NUMBER: u32 = 16;
    type Input = &'static str;
    type OutputPart1 = String;
    type OutputPart2 = String;

    fn create(input: &str) -> DayCreationResult<Day16> {
        let dance_notes = read_to_string(input)?;

        Ok(Day16 { dance_notes })
    }

    fn get_solutions(&self) -> (Option<String>, Option<String>) {
        let parse_res: Result<(Vec<DanceMove>, &str), _> =  sep_by(dance_move(), char(','))
            .parse(self.dance_notes.as_ref());

        match parse_res {
            Err(_) => (None, None),
            Ok((dance_moves, _)) => {
                let mut choreo = Choreography::starting_formation();
                dance_moves.iter().for_each(|dance_move| choreo.add_move(dance_move));

                (Some(choreo.dance(1)), Some(choreo.dance(1_000_000_000)))
            },
        }
    }
}
