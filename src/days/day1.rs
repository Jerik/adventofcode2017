use crate::aoc_helper::core::*;
use std::fs::read_to_string;

pub struct Day1 {
    sequence: String,
}

impl Day for Day1 {
    const NUMBER: u32 = 1;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day1> {
        let sequence = read_to_string(input)?;

        Ok(Day1 { sequence })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let numbers: Vec<u32> = self.sequence.chars().filter_map(|ch| format!("{}", ch).parse().ok()).collect();
        let length = numbers.len();
        let half = length / 2;

        let (sum1, sum2) = numbers.iter().enumerate().fold((0, 0), |(acc1, acc2), (i, &num)| {
            (acc1 + if num == numbers[(i + 1) % length] { num } else { 0 },
             acc2 + if num == numbers[(i + half) % length] { num } else { 0 })
        });

        (Some(sum1), Some(sum2))
    }
}
