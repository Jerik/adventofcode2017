use crate::aoc_helper::core::*;
use std::{fs::read_to_string, iter::FusedIterator};

#[derive(Clone)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

struct Position {
    line_index: usize,
    char_index: usize,
}

impl Position {
    fn new(line_index: usize, char_index: usize) -> Position {
        Position {
            line_index,
            char_index,
        }
    }

    fn get_movement_options(&self, original_direction: &Direction) -> [(Position, Direction); 3] {
        let up = (Position::new(self.line_index - 1, self.char_index), Direction::Up);
        let down = (Position::new(self.line_index + 1, self.char_index), Direction::Down);
        let left = (Position::new(self.line_index, self.char_index - 1), Direction::Left);
        let right = (Position::new(self.line_index, self.char_index + 1), Direction::Right);

        match original_direction {
            Direction::Up => [up, left, right],
            Direction::Down => [down, left, right],
            Direction::Left => [up, down, left],
            Direction::Right => [up, down, right],
        }
    }
}

struct Navigation {
    char_table: Vec<Vec<char>>,
    current_position: Position,
    direction: Direction,
}

impl Navigation {
    fn try_init(char_table: Vec<Vec<char>>) -> Option<Navigation> {
        let start_char_index = char_table[0].iter().position(|&ch| ch == '|')?;

        Some(Navigation {
            char_table,
            current_position: Position::new(0, start_char_index),
            direction: Direction::Down,
        })
    }

    fn get_char_at(&self, position: &Position) -> Option<char> {
        self.char_table.get(position.line_index)?.get(position.char_index).cloned()
    }
}

impl Iterator for Navigation {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        let current_char = self.get_char_at(&self.current_position).filter(|ch| !ch.is_whitespace())?;

        if current_char == '+' {
            let options = self.current_position.get_movement_options(&self.direction);

            let viable_options: Vec<&Direction> = options.iter().filter_map(|(pos, dir)| {
                self.get_char_at(pos).filter(|ch| !ch.is_whitespace()).map(|_| dir)
            }).collect();

            if viable_options.len() == 1 {
                self.direction = viable_options[0].clone()
            }
        }

        match self.direction {
            Direction::Up => self.current_position.line_index -= 1,
            Direction::Down => self.current_position.line_index += 1,
            Direction::Left => self.current_position.char_index -= 1,
            Direction::Right => self.current_position.char_index += 1,
        }

        Some(current_char)
    }
}

impl FusedIterator for Navigation {}

pub struct Day19 {
    diagram: String,
}

impl Day for Day19 {
    const NUMBER: u32 = 19;
    type Input = &'static str;
    type OutputPart1 = String;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day19> {
        let diagram = read_to_string(input)?;

        Ok(Day19 { diagram })
    }

    fn get_solutions(&self) -> (Option<String>, Option<u32>) {
        let char_table: Vec<Vec<char>> = self.diagram.lines().map(|line| line.chars().collect()).collect();
        let navigation_opt = Navigation::try_init(char_table);

        match navigation_opt {
            None => (None, None),
            Some(navigation) => {
                let (sequence, number_of_steps) = navigation.fold((String::new(), 0), |(s, n), ch| {
                    (if ch.is_alphabetic() { format!("{}{}", s, ch) } else { s }, n + 1)
                });

                (Some(sequence), Some(number_of_steps))
            },
        }
    }
}
