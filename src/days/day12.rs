use crate::aoc_helper::{core::*, util::extract_group, parse::unsigned_integer};
use std::{fs::read_to_string, collections::HashMap};
use combine::{
    Parser, ParseError, Stream, sep_by,
    parser::char::{space, string, char}
};

fn program_connections<I>() -> impl Parser<Input = I, Output = (u32, Vec<u32>)>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    (
        unsigned_integer(),
        space().and(string("<->")).and(space())
        .with(sep_by(unsigned_integer(), char(',').and(space()))),
    )
}

pub struct Day12 {
    connection_notes: String,
}

impl Day for Day12 {
    const NUMBER: u32 = 12;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day12> {
        let connection_notes = read_to_string(input)?;

        Ok(Day12 { connection_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut connection_map = self.connection_notes.lines().filter_map(|line| {
            program_connections().parse(line).map(|(pc, _)| pc).ok()
        }).collect::<HashMap<u32, Vec<u32>>>();

        let mut number_of_groups = 0;

        let zero_group_length_opt = extract_group(&mut connection_map, 0).map(|group| {
            number_of_groups += 1;
            group.len() as u32
        });

        while let Some(&representative) = connection_map.keys().next() {
            extract_group(&mut connection_map, representative);
            number_of_groups += 1;
        }

        (zero_group_length_opt, Some(number_of_groups))
    }
}
