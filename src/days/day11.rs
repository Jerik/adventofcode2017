use crate::aoc_helper::core::*;
use std::fs::read_to_string;

enum Direction {
    North,
    Northeast,
    Southeast,
    South,
    Southwest,
    Northwest,
}

struct HexPosition {
    x: i32,
    y: i32,
}

impl HexPosition {
    fn go_step(&mut self, direction: &Direction) {
        match direction {
            Direction::North => self.y += 1,
            Direction::Northeast => self.x += 1,
            Direction::Southeast => { self.x += 1; self.y -= 1; },
            Direction::South => self.y -= 1,
            Direction::Southwest => self.x -= 1,
            Direction::Northwest => { self.x -= 1; self.y += 1; },
        }
    }

    fn get_distance(&self) -> u32 {
        ((self.x.abs() + self.y.abs() + (self.x + self.y).abs()) / 2) as u32
    }
}

pub struct Day11 {
    direction_notes: String,
}

impl Day for Day11 {
    const NUMBER: u32 = 11;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day11> {
        let direction_notes = read_to_string(input)?;

        Ok(Day11 { direction_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut position = HexPosition { x: 0, y: 0 };

        let max_distance = self.direction_notes.trim().split_terminator(',').filter_map(|s| {
            match s {
                "n" => Some(Direction::North),
                "ne" => Some(Direction::Northeast),
                "se" => Some(Direction::Southeast),
                "s" => Some(Direction::South),
                "sw" => Some(Direction::Southwest),
                "nw" => Some(Direction::Northwest),
                _ => None,
            }
        }).fold(0, |max, dir| {
            position.go_step(&dir);
            let distance = position.get_distance();
            
            if distance > max { distance } else { max }
        });

        (Some(position.get_distance()), Some(max_distance))
    }
}
