use crate::aoc_helper::core::*;

pub struct Day17 {
    step: usize,
}

impl Day for Day17 {
    const NUMBER: u32 = 17;
    type Input = usize;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: usize) -> DayCreationResult<Day17> {
        Ok(Day17 { step: input })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut spin_lock = vec![0];

        let i = (1..=2017).fold(1, |current_pos, n| {
            let next_pos = (current_pos + self.step) % n + 1;
            spin_lock.insert(next_pos, n);

            next_pos
        });
        
        let value_after_2017 = spin_lock[(i + 1) % 2018];

        let (_, value_after_0) = (1..=50_000_000).fold((1, 1), |(current_pos, val), n| {
            let next_pos = (current_pos + self.step) % n + 1;

            (next_pos, if next_pos == 1 { n } else { val })
        });

        (Some(value_after_2017 as u32), Some(value_after_0 as u32))
    }
}
