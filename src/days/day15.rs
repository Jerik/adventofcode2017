use crate::aoc_helper::{core::*, parse::unsigned_integer};
use std::fs::read_to_string;
use combine::{Parser, parser::repeat::skip_until};

#[derive(Clone, Copy)]
struct Generator {
    value: u64,
    multiplier: u64,
}

impl Generator {
    fn new<T: Into<u64>>(starting_value: T, multiplier: u64) -> Generator {
        Generator {
            value: starting_value.into(),
            multiplier,
        }
    }
}

impl Iterator for Generator {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        self.value = (self.value * self.multiplier) % 2_147_483_647;

        Some(self.value)
    }
}

pub struct Day15 {
    value_notes: String,
}

impl Day for Day15 {
    const NUMBER: u32 = 15;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day15> {
        let value_notes = read_to_string(input)?;

        Ok(Day15 { value_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut get_next_integer = skip_until(unsigned_integer()).with(unsigned_integer());

        let value_res = get_next_integer.parse(self.value_notes.as_ref())
                .and_then(|(value_a, remaining_input): (u32, &str)| {
            get_next_integer.parse(remaining_input).map(|(value_b, _)| (value_a, value_b))
        });

        match value_res {
            Err(_) => (None, None),
            Ok((value_a, value_b)) => {
                let gen_a = Generator::new(value_a, 16807);
                let gen_b = Generator::new(value_b, 48271);
                let is_match = |&(a, b): &(u64, u64)| a & 0xFFFF == b & 0xFFFF;

                let count1 = gen_a.zip(gen_b).take(40_000_000).filter(is_match).count();
                
                let count2 = gen_a.filter(|a| a % 4 == 0)
                    .zip(gen_b.filter(|b| b % 8 == 0))
                    .take(5_000_000).filter(is_match).count();

                (Some(count1 as u32), Some(count2 as u32))
            }
        }
    }
}
