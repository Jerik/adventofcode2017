use crate::aoc_helper::{core::*, parse::integer};
use std::{
    fs::read_to_string, collections::HashMap, thread,
    time::Duration, sync::mpsc::{self, Sender, Receiver}
};
use combine::{
    Parser, ParseError, Stream, choice, attempt,
    parser::char::{space, letter, string}
};

#[derive(Clone)]
enum Value {
    Number(i64),
    Register(char),
}

impl Value {
    fn get(&self, registers: &mut HashMap<char, i64>) -> i64 {
        match *self {
            Value::Number(x) => x,
            Value::Register(ch) => *registers.entry(ch).or_insert(0),
        }
    }
}

#[derive(Clone)]
enum Instruction {
    Snd(Value),
    Rcv(Value),
    Other(BaseInstruction),
}

#[derive(Clone)]
enum BaseInstruction {
    Set(char, Value),
    Add(char, Value),
    Mul(char, Value),
    Mod(char, Value),
    Jump(Value, Value),
}

impl BaseInstruction {
    fn execute(&self, registers: &mut HashMap<char, i64>, index: usize) -> Option<usize> {
        match self {
            BaseInstruction::Set(ch, val) => {
                let value = val.get(registers);
                registers.insert(*ch, value);
            },
            BaseInstruction::Add(ch, val) => {
                let value = val.get(registers);
                *registers.entry(*ch).or_insert(0) += value;
            },
            BaseInstruction::Mul(ch, val) => {
                let value = val.get(registers);
                *registers.entry(*ch).or_insert(0) *= value;
            },
            BaseInstruction::Mod(ch, val) => {
                let value = val.get(registers);
                *registers.entry(*ch).or_insert(0) %= value;
            },
            BaseInstruction::Jump(val1, val2) => {
                if val1.get(registers) > 0 {
                    let value = val2.get(registers);

                    if value < 0 {
                        return index.checked_sub(value.abs() as usize);
                    } else {
                        return Some(index + value as usize);
                    }
                }
            },
        }

        Some(index + 1)
    }
}

struct Program {
    id: u32,
    instructions: Vec<Instruction>,
    sender: Sender<i64>,
    receiver: Receiver<i64>,
}

impl Program {
    fn new(id: u32, instructions: Vec<Instruction>, sender: Sender<i64>, receiver:Receiver<i64>) -> Program {
        Program {
            id,
            instructions,
            sender,
            receiver,
        }
    }

    fn recover_first_sound(&self) -> Option<i64> {
        let mut registers: HashMap<char, i64> = HashMap::new();
        let mut index = 0;
        let mut sound = None;

        while let Some(instruction) = self.instructions.get(index) {
            match instruction {
                Instruction::Snd(val) => {
                    sound = Some(val.get(&mut registers));
                    index += 1;
                },
                Instruction::Rcv(val) => {
                    if val.get(&mut registers) != 0 && sound.is_some() {
                        break;
                    } else {
                        index += 1;
                    }
                },
                Instruction::Other(base_instruction) => {
                    if let Some(i) = base_instruction.execute(&mut registers, index) {
                        index = i;
                    } else {
                        break;
                    }
                },
            }
        }

        sound
    }

    fn count_sent_values(&self) -> u32 {
        let mut registers: HashMap<char, i64> = [('p', i64::from(self.id))].iter().cloned().collect();
        let mut index = 0;
        let mut counter = 0;

        while let Some(instruction) = self.instructions.get(index) {
            match instruction {
                Instruction::Snd(val) => {
                    self.sender.send(val.get(&mut registers)).unwrap();
                    counter += 1;
                    index += 1;
                },
                Instruction::Rcv(val) => {
                    if let Ok(received_value) = self.receiver.recv_timeout(Duration::new(3, 0)) {
                        if let Value::Register(ch) = val {
                            registers.insert(*ch, received_value);
                        }

                        index += 1;
                    } else {
                        break;
                    }
                },
                Instruction::Other(base_instruction) => {
                    if let Some(i) = base_instruction.execute(&mut registers, index) {
                        index = i;
                    } else {
                        break;
                    }
                }
            }
        }

        counter
    }
}

fn value<I>() -> impl Parser<Input = I, Output = Value>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((
        letter().map(Value::Register),
        integer().map(|x| Value::Number(i64::from(x))),
    ))
}

fn instruction<I>() -> impl Parser<Input = I, Output = Instruction>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((
        attempt(string("snd").and(space()).with(value()).map(Instruction::Snd)),
        attempt(string("rcv").and(space()).with(value()).map(Instruction::Rcv)),
        attempt(string("set").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Other(BaseInstruction::Set(ch, val))
        })),
        attempt(string("add").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Other(BaseInstruction::Add(ch, val))
        })),
        attempt(string("mul").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Other(BaseInstruction::Mul(ch, val))
        })),
        attempt(string("mod").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| { 
            Instruction::Other(BaseInstruction::Mod(ch, val))
        })),
        attempt(string("jgz").and(space()).with(value().skip(space()).and(value())).map(|(val1, val2)| {
            Instruction::Other(BaseInstruction::Jump(val1, val2))
        })),
    ))
}

pub struct Day18 {
    instruction_notes: String,
}

impl Day for Day18 {
    const NUMBER: u32 = 18;
    type Input = &'static str;
    type OutputPart1 = i64;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day18> {
        let instruction_notes = read_to_string(input)?;

        Ok(Day18 { instruction_notes })
    }

    fn get_solutions(&self) -> (Option<i64>, Option<u32>) {
        let instructions: Vec<Instruction> = self.instruction_notes.lines().filter_map(|line| {
            instruction().parse(line).map(|(instr, _)| instr).ok()
        }).collect();

        let (sender1, receiver2) = mpsc::channel();
        let (sender2, receiver1) = mpsc::channel();

        let program1 = Program::new(0, instructions.clone(), sender1, receiver1);
        let program2 = Program::new(1, instructions.clone(), sender2, receiver2);

        let recovered_sound = program1.recover_first_sound();

        thread::spawn(move || program1.count_sent_values());
        let handle = thread::spawn(move || program2.count_sent_values());

        (recovered_sound, handle.join().ok())
    }
}
