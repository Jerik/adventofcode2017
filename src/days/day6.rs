use crate::aoc_helper::core::*;
use std::{fs::read_to_string, collections::HashMap};

pub struct Day6 {
    banks: String,
}

impl Day for Day6 {
    const NUMBER: u32 = 6;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day6> {
        let banks = read_to_string(input)?;

        Ok(Day6 { banks })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let mut state: Vec<u32> = self.banks.split_whitespace().filter_map(|num| num.parse().ok()).collect();

        if state.is_empty() {
            (None, None)
        } else {
            let length = state.len();

            let mut seen_states: HashMap<Vec<u32>, u32> = HashMap::new();
            let mut counter = 0;

            while seen_states.get(&state).is_none() {
                seen_states.insert(state.clone(), counter);

                let max_blocks_index = state.iter().enumerate().fold(0, |max_i, (i, &blocks)| {
                    if blocks > state[max_i] { i } else { max_i }
                });

                let max_blocks = state[max_blocks_index] as usize;
                let redistributions = [length, max_blocks].iter().cloned().min().unwrap();

                state[max_blocks_index] = 0;

                for r in 1..=redistributions {
                    state[(max_blocks_index + r) % length] += 1 + ((max_blocks - r) / length) as u32;
                }

                counter += 1;
            }

            let cycle = counter - seen_states[&state];

            (Some(counter), Some(cycle))
        }
    }
}
