use crate::aoc_helper::{core::*, parse::integer};
use std::{fs::read_to_string, collections::HashMap};
use primal::Sieve;
use combine::{
    Parser, ParseError, Stream, choice, attempt,
    parser::char::{space, letter, string}
};

enum Value {
    Number(i64),
    Register(char),
}

impl Value {
    fn get(&self, registers: &mut HashMap<char, i64>) -> i64 {
        match *self {
            Value::Number(x) => x,
            Value::Register(ch) => *registers.entry(ch).or_insert(0),
        }
    }
}

enum Instruction {
    Set(char, Value),
    Sub(char, Value),
    Mul(char, Value),
    Jump(Value, Value),
}

impl Instruction {
    fn execute(&self, registers: &mut HashMap<char, i64>, index: usize) -> Option<usize> {
        match self {
            Instruction::Set(ch, val) => {
                let value = val.get(registers);
                registers.insert(*ch, value);
            },
            Instruction::Sub(ch, val) => {
                let value = val.get(registers);
                *registers.entry(*ch).or_insert(0) -= value;
            },
            Instruction::Mul(ch, val) => {
                let value = val.get(registers);
                *registers.entry(*ch).or_insert(0) *= value;
            },
            Instruction::Jump(val1, val2) => {
                if val1.get(registers) != 0 {
                    let value = val2.get(registers);

                    if value < 0 {
                        return index.checked_sub(value.abs() as usize);
                    } else {
                        return Some(index + value as usize);
                    }
                }
            },
        }

        Some(index + 1)
    }
}

fn get_num_of_muls(instructions: &[Instruction]) -> u32 {
    let mut registers: HashMap<char, i64> = HashMap::new();
    let mut index = 0;
    let mut num_of_muls = 0;

    while let Some(instruction) = instructions.get(index) {
        if let Instruction::Mul(_, _) = instruction {
            num_of_muls += 1;
        }

        match instruction.execute(&mut registers, index) {
            Some(i) => index = i,
            None => break,
        }
    }

    num_of_muls
}

fn get_register_h_value(instructions: &[Instruction]) -> Option<u32> {
    let mut registers: HashMap<char, i64> = [('a', 1)].iter().cloned().collect();
    let mut index = 0;

    // Register b is initialized after the first 6 instructions.
    while let Some(instruction) = instructions.get(index) {
        match instruction.execute(&mut registers, index) {
            Some(i) if i < 6 => index = i,
            _ => break,
        }
    }

    // The program then searches for composite numbers in the range of b to b + 17000 with a step of 17.
    let b = *registers.get(&'b').filter(|&&n| n >= 0)? as usize;
    let c = b + 17000;
    let sieve = Sieve::new(c);

    Some((b..=c).step_by(17).filter(|&n| !sieve.is_prime(n)).count() as u32)
}

fn value<I>() -> impl Parser<Input = I, Output = Value>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((
        letter().map(Value::Register),
        integer().map(|x| Value::Number(i64::from(x))),
    ))
}

fn instruction<I>() -> impl Parser<Input = I, Output = Instruction>
where I: Stream<Item = char>, I::Error: ParseError<I::Item, I::Range, I::Position>
{
    choice((
        attempt(string("set").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Set(ch, val)
        })),
        attempt(string("sub").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Sub(ch, val)
        })),
        attempt(string("mul").and(space()).with(letter().skip(space()).and(value())).map(|(ch, val)| {
            Instruction::Mul(ch, val)
        })),
        attempt(string("jnz").and(space()).with(value().skip(space()).and(value())).map(|(val1, val2)| {
            Instruction::Jump(val1, val2)
        })),
    ))
}

pub struct Day23 {
    instruction_notes: String,
}

impl Day for Day23 {
    const NUMBER: u32 = 23;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day23> {
        let instruction_notes = read_to_string(input)?;

        Ok(Day23 { instruction_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let instructions: Vec<Instruction> = self.instruction_notes.lines().filter_map(|line| {
            instruction().parse(line).map(|(instr, _)| instr).ok()
        }).collect();

        let num_of_muls = get_num_of_muls(&instructions);
        let part2 = get_register_h_value(&instructions);

        (Some(num_of_muls), part2)
    }
}
