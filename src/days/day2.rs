use crate::aoc_helper::core::*;
use std::fs::read_to_string;

pub struct Day2 {
    spreadsheet: String,
}

impl Day for Day2 {
    const NUMBER: u32 = 2;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = i32;

    fn create(input: &str) -> DayCreationResult<Day2> {
        let spreadsheet = read_to_string(input)?;

        Ok(Day2 { spreadsheet })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<i32>) {
        let (sum1, sum2) = self.spreadsheet.lines().fold((0, 0), |(acc1, acc2), line| {
            let numbers: Vec<i32> = line.split_whitespace().filter_map(|s| s.parse().ok()).collect();
            
            if numbers.is_empty() {
                (acc1, acc2)
            } else {
                let mut min = numbers[0];
                let mut max = numbers[0];
                let mut quotient = None;

                for (i, &num1) in numbers.iter().enumerate() {
                    if num1 < min { min = num1; }

                    if num1 > max { max = num1; }

                    if quotient.is_none() {
                        for num2 in &numbers[i+1..] {
                            if num1 % num2 == 0 {
                                quotient = Some(num1 / num2);
                                break;
                            } else if num2 % num1 == 0 {
                                quotient = Some(num2 / num1);
                                break;
                            }
                        }
                    }
                }

                (acc1 + max - min, acc2 + quotient.unwrap_or(0))
            }
        });

        (Some(sum1 as u32), Some(sum2))
    }
}
