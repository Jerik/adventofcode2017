use crate::aoc_helper::core::*;
use std::{fs::read_to_string, collections::VecDeque, cmp::Ordering};

struct Bridge<'a> {
    components: &'a [(u32, u32)],
    comp_is_used: Vec<bool>,
    connection_port: u32,
    strength: u32,
    length: u32,
}

impl<'a> Bridge<'a> {
    fn new(components: &[(u32, u32)], connection_port: u32, strength: u32, length: u32) -> Bridge {
        Bridge {
            components,
            comp_is_used: vec![false; components.len()],
            connection_port,
            strength,
            length,
        }
    }

    fn with_comp_is_used(mut self, comp_is_used: Vec<bool>) -> Bridge<'a> {
        self.comp_is_used = comp_is_used;

        self
    }

    fn get_child_bridge(&self, new_comp_index: usize, child_connection_port: u32) -> Bridge<'a> {
        let child_strength = self.strength + self.connection_port + child_connection_port;
        let child_comp_is_used = self.comp_is_used.iter().enumerate().map(|(i, &b)| {
            if i == new_comp_index { true } else { b }
        }).collect();

        Bridge::new(self.components, child_connection_port, child_strength, self.length + 1)
            .with_comp_is_used(child_comp_is_used)
    }

    fn get_all_children_bridges(self) -> Vec<Bridge<'a>> {
        let mut bridges = Vec::new();

        for (i, comp) in self.components.iter().enumerate().filter(|&(i, _)| !self.comp_is_used[i]) {
            match *comp {
                (port1, port2) if port1 == self.connection_port => {
                    bridges.push(self.get_child_bridge(i, port2));
                },
                (port1, port2) if port2 == self.connection_port => {
                    bridges.push(self.get_child_bridge(i, port1));
                },
                _ => {},
            }
        }

        bridges
    }
}

fn build_bridges(initial_bridge: Bridge) -> (u32, u32) {
    let mut queue: VecDeque<Bridge> = VecDeque::new();
    let mut max_strength = 0;
    let mut max_length_with_strength = (0, 0);

    queue.push_front(initial_bridge);

    while let Some(bridge) = queue.pop_front() {
        if bridge.strength > max_strength {
            max_strength = bridge.strength;
        }

        match bridge.length.cmp(&max_length_with_strength.0) {
            Ordering::Greater => max_length_with_strength = (bridge.length, bridge.strength),
            Ordering::Equal if bridge.strength > max_length_with_strength.1 => {
                max_length_with_strength.1 = bridge.strength;
            },
            _ => {},
        }

        for child_bridge in bridge.get_all_children_bridges() {
            queue.push_back(child_bridge);
        }
    }

    (max_strength, max_length_with_strength.1)
}

pub struct Day24 {
    component_notes: String,
}

impl Day for Day24 {
    const NUMBER: u32 = 24;
    type Input = &'static str;
    type OutputPart1 = u32;
    type OutputPart2 = u32;

    fn create(input: &str) -> DayCreationResult<Day24> {
        let component_notes = read_to_string(input)?;

        Ok(Day24 { component_notes })
    }

    fn get_solutions(&self) -> (Option<u32>, Option<u32>) {
        let components: Vec<(u32, u32)> = self.component_notes.lines().filter_map(|line| {
            let mut split = line.split_terminator('/');

            Some((split.next()?.parse().ok()?, split.next()?.parse().ok()?))
        }).collect();

        let initial_bridge = Bridge::new(&components, 0, 0, 0);
        let (solution1, solution2) = build_bridges(initial_bridge);

        (Some(solution1), Some(solution2))
    }
}
